#! /usr/bin/env python

import saga_api

reference_points = '/mnt/lud11/karger/arctic_sdms/reference.shp'
input_directory = '/mnt/lud11/karger/arctic_sdms/baltic/png/'
# define functions
def Load_Tool_Libraries(Verbose):
    saga_api.SG_UI_Msg_Lock(True)
    if os.name == 'nt':    # Windows
        os.environ['PATH'] = os.environ['PATH'] + ';' + os.environ['SAGA_32'] + '/dll'
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory(os.environ['SAGA_32' ] + '/tools', False)
    else:                  # Linux
        saga_api.SG_Get_Tool_Library_Manager().Add_Directory('/usr/local/lib/saga/', False)        # Or set the Tool directory like this!
    saga_api.SG_UI_Msg_Lock(False)

    if Verbose == True:
                print 'Python - Version ' + sys.version
                print saga_api.SAGA_API_Get_Version()
                print 'number of loaded libraries: ' + str(saga_api.SG_Get_Tool_Library_Manager().Get_Count())
                print

    return saga_api.SG_Get_Tool_Library_Manager().Get_Count()


def load_sagadata(path_to_sagadata):

    saga_api.SG_Set_History_Depth(0)    # History will not be created
    saga_api_dataobject = 0             # initial value

    # CSG_Grid -> Grid
    if any(s in path_to_sagadata for s in (".sgrd", ".sg-grd", "sg-grd-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Grid(unicode(path_to_sagadata))

    # CSG_Grids -> Grid Collection
    if any(s in path_to_sagadata for s in ("sg-gds", "sg-gds-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Grids(unicode(path_to_sagadata))

    # CSG_Table -> Table
    if any(s in path_to_sagadata for s in (".txt", ".csv", ".dbf")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Table(unicode(path_to_sagadata))

    # CSG_Shapes -> Shapefile
    if '.shp' in path_to_sagadata:
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_Shapes(unicode(path_to_sagadata))

    # CSG_PointCloud -> Point Cloud
    if any(s in path_to_sagadata for s in (".spc", ".sg-pts", ".sg-pts-z")):
        saga_api_dataobject = saga_api.SG_Get_Data_Manager().Add_PointCloud(unicode(path_to_sagadata))

    if saga_api_dataobject == None or saga_api_dataobject.is_Valid() == 0:
        print 'ERROR: loading [' + path_to_sagadata + ']'
        return 0

    print 'File: [' + path_to_sagadata + '] has been loaded'
    return saga_api_dataobject


def Run_Rectify_Grid(shp, image):
    #_____________________________________
    Tool = saga_api.SG_Get_Tool_Library_Manager().Get_Tool('pj_georeference', '1')
    if Tool == None:
        print('Failed to create tool: Rectify Grid')
        return False

    Tool.Set_Parameter('REF_SOURCE', shp)
    Tool.Set_Parameter('REF_TARGET', 'Shapes input, optional')
    Tool.Set_Parameter('XFIELD', '<no attributes>')
    Tool.Set_Parameter('YFIELD', '<no attributes>')
    Tool.Set_Parameter('METHOD', 'Automatic')
    Tool.Set_Parameter('ORDER', 3)
    Tool.Set_Parameter('GRID', image)
    Tool.Set_Parameter('RESAMPLING', 'B-Spline Interpolation')
    Tool.Set_Parameter('BYTEWISE', False)
    Tool.Set_Parameter('TARGET.DEFINITION', 'user defined')
    Tool.Set_Parameter('TARGET.USER_SIZE', 1.000000)
    Tool.Set_Parameter('TARGET.USER_XMIN', 0.000000)
    Tool.Set_Parameter('TARGET.USER_XMAX', 100.000000)
    Tool.Set_Parameter('TARGET.USER_YMIN', 0.000000)
    Tool.Set_Parameter('TARGET.USER_YMAX', 100.000000)
    Tool.Set_Parameter('TARGET.USER_COLS', 101)
    Tool.Set_Parameter('TARGET.USER_ROWS', 101)
    Tool.Set_Parameter('TARGET.USER_FITS', 'nodes')
    Tool.Set_Parameter('TARGET.TEMPLATE', 'Grid input, optional')

    if Tool.Execute() == False:
        print('failed to execute tool: ' + Tool.Get_Name().c_str())
        return False

    #_____________________________________
    # Save results to file:
    ds = Tool.Get_Parameter(saga_api.CSG_String('TARGET_OUT_GRID')).asGrid()

    return ds


def main():
    for timestep in range(1,161):
        gridimage = input_directory + 'target_' + range + '.png'




