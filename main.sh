#!/bin/bash

#SBATCH --job-name=artic_sdms
#SBATCH --partition=node
#SBATCH --qos=normal
#SBATCH --account=node
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH --mem-per-cpu=3000
#SBATCH --time=696:24:15
#SBATCH --output=~/aSDM_%A_%a.out

OUTPUT=~/output/ # change this to your output directory
SPECIES=~/data/ # this directory needs to contain the presences from GBIF
INPUT=/storage/karger/chelsa/orig_ts_100a_21BP_P/bio/
FUNCTION=~/arctic_sdms/helper/ # set this to the directory where arctic_sdm/helper can be found

srun singularity exec -B /storage /storage/karger/singularity/ubuntu_R.4.x.cont Rscript --vanilla ~/arctic_sdms/main.R $SLURM_ARRAY_TASK_ID ${OUTPUT} ${SPECIES} ${INPUT} ${FUNCTION}
